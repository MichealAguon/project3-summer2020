﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public string gameState = "Start Screen";
    private static GameManager instance;
    public GameObject titleScreen;
    public GameObject gameUI;
    public GameObject security;
    public GameObject player;
    public GameObject playerPrefab;
    public GameObject playerSpawnPoint;
    public GameObject playerDeathScreen;
    public GameObject gameOverScreen;
    public int lives = 0;
    public int maxLives = 3;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogWarning("Attempted tocreate a second game manager.");
            Destroy(this.gameObject);
        }
    }

    public void ChangeState(string newState)
    {
        gameState = newState;
    }

    public void StartGame()
    {
        Debug.Log("Game has started");
        ChangeState("Initialize Game");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (gameState == "Start Screen")
        {
            // Do the state behavior
            StartScreen();
            // Cheack for transitions
            // This transitions on a button click
        }
        else if (gameState == "Initialize Game")
        {
            // Do the state behavior
            InitializeGame();
            // Cheack for transitions
            ChangeState("Spawn Player");
        }
        else if (gameState == "Spawn Player")
        {
            // Do the state behavior
            SpawnPlayer();
            // Cheack for transitions
            ChangeState("Gameplay");
        }
        else if (gameState == "Gameplay")
        {
            // Do the state behavior
            Gameplay();
            // Cheack for transitions
            if (player == null && lives > 0)
            {
                ChangeState("Player Death");
            }
            else if (player == null && lives <=0)
            {
                ChangeState("Game Over");
            }
        }
        else if (gameState == "Player Death")
        {
            // Do the state behavior
            PlayerDeath();
            // Cheack for transitions
            if (Input.anyKeyDown)
            {
                ChangeState("Spawn Player");
            }
        }
        else if (gameState == "Game Over")
        {
            // Do the state behavior
            GameOver();
            // Cheack for transitions
            if (Input.anyKeyDown)
            {
                ChangeState("Start Screen");
            }
        }
        else
        {
            Debug.LogWarning("Game manager tried to change to non-existent state: " + gameState);
        }
    }

    public void StartScreen()
    {
        // Show the menu
        if (!titleScreen.activeSelf)
        {
            titleScreen.SetActive(true);
        }
        if (gameOverScreen.activeSelf)
        {
            gameOverScreen.SetActive(false);
        }
    }

    public void InitializeGame()
    {
        // Reset all variables
        // TODO: Reset variables in initializegame
        // Turn off the menu
        titleScreen.SetActive(false);
        // Turn on the game ui
        gameUI.SetActive(true);
        lives = maxLives;
    }

    public void SpawnPlayer()
    {
        // Add the player to the world
        player = Instantiate(playerPrefab, playerSpawnPoint.transform.position, Quaternion.identity);
        if (playerDeathScreen.activeSelf)
        {
            playerDeathScreen.SetActive(false);
        }
        lives--;
    }

    public void Gameplay()
    {

    }

    public void PlayerDeath()
    {
        if (!playerDeathScreen.activeSelf)
        {
            playerDeathScreen.SetActive(true);
        }
    }

    public void GameOver()
    {
        if (gameUI.activeSelf)
        {
            gameUI.SetActive(false);
        }
        if (!gameOverScreen.activeSelf)
        {
            gameOverScreen.SetActive(true);
        }
    }
}
