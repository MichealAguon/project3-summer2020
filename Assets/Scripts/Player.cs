﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Transform tf;
    public float turnSpeed = 90f; // Degrees per second
    public float moveSpeed = 10f; //Meters per second
  //public GameObject bulletPrefab;

//public float bulletSpeed = 12f;
    void Start()
    {
        //GameManager.instance.player = this.gameObject;
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleRotation();
        if (Input.GetKey(KeyCode.UpArrow))
        {
            tf.position += tf.right * moveSpeed * Time.deltaTime;
        }
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
          //  Shoot();
        //}
    }

    //public void Shoot()
    //{
        //GameObject bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
      //  bullet.GetComponent<Bullet>().bulletSpeed = bulletSpeed;
    //}

    public void HandleRotation()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            tf.Rotate(0, 0, -turnSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D otherObject)
    {
        // If the player runs into security, they should die.
        Die();
    }

    void Die()
    {
        Destroy(this.gameObject);
    }

    void OnDestroy()
    {
        GameManager.Instance.player = null;
    }
}
